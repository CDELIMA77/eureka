FROM openjdk:8
WORKDIR /app
COPY /target/eureka-*.jar api-eureka.jar

CMD ["java", "-jar", "api-eureka.jar"]
